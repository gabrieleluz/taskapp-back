<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'NBZ',
            'username' => 'nbz',
            'password' => $password = bcrypt('123mil'),
            'remember_token' => str_random(10),
        ]);
        factory(App\User::class, 14)->create();
    }
}
