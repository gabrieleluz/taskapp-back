<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'status' => $faker->randomElement($array = array ('done','todo')),
        'due' => $faker->dateTimeThisMonth($max = 'now', $timezone = null) ,
        'categories_id' => $faker->numberBetween($min = 1, $max = 14),
    ];
});
