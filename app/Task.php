<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title', 'description', 'status', 'due', 'categories_id', 'users_id'
    ];

    public function category() {
        return $this->hasOne('App\Category', 'categories_id');
    }

    public function user() {
        return $this->belogsTo('App\User', 'users_id');
    }
}
