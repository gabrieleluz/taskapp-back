<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title'];

    function tasks() {
        return $this->hasMany('App\Task', 'categories_id', 'id');
    }
}
