<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Task;
use App\Http\Controllers\Auth;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\Task as TaskResource;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = $request->user()->id;
        $categories = Category::where('users_id', $user_id)->get();
        return CategoryResource::collection($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = $request->isMethod('put') ? Category::findOrFail($request->id) : new Category;
        $category->name = $request->input('name');
        $category->users_id = $request->user()->id;
        if($category->save()) {
            return new CategoryResource($category);
        }
    }

    /**
     * Displays tasks related to the category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showTask($id)
    {
        $tasks = Task::where('categories_id', $id)->get();
        return TaskResource::collection($tasks);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if($category->delete()) {
            return new CategoryResource($category);
        }
    }
}
