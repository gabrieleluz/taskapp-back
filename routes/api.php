<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        Route::get('categories', 'CategoryController@index');
        Route::get('category/{id}', 'CategoryController@showTask');
        Route::post('category', 'CategoryController@store');
        Route::delete('category/{id}', 'CategoryController@destroy');

        Route::post('task', 'TaskController@store');
        Route::get('task/{id}', 'TaskController@updateStatus');
        Route::delete('task/{id}', 'TaskController@destroy');
        Route::put('task', 'TaskController@store');
    });
});

